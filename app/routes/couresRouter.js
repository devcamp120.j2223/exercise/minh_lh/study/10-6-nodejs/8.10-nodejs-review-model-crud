//khai báo thư viện express
const express = require('express');
//khai báo middleware
const { courseMiddleware } = require('../middlewares/courseMiddleware');
//import module course controller
const { getAllCourses, getCourseById, createCourse, updateCourseById, deleteCourseById } = require('../controllers/courseController');

//tạo router
const courseRouter = express.Router();

//sủ dụng middle ware
courseRouter.use(courseMiddleware);

//get all courses
courseRouter.get('/courses', getAllCourses);

//get a course by id
courseRouter.get('/courses/:courseid', getCourseById)

//create a course
courseRouter.post('/courses', createCourse);

//update a course by id
courseRouter.put('/courses/:courseid', updateCourseById);

//delete a course by id
courseRouter.delete('/courses/:courseid', deleteCourseById);

module.exports = { courseRouter };