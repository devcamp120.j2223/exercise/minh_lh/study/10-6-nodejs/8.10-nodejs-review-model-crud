//khai báo thư viện express
const express = require('express');
const reviewMiddleware = require('../middlewares/reviewMiddleware');

const { createReviewOfCourse, getAllReviewOfCourse, getReviewById, deleteReviewById, updateReviewById } = require('../controllers/reviewController');

//tạo router
const reviewRouter = express.Router();

//sủ dụng middle ware
reviewRouter.use(reviewMiddleware);

//create a review
reviewRouter.post('/courses/:courseId/reviews', createReviewOfCourse);

// get all review
reviewRouter.get('/courses/:courseId/reviews', getAllReviewOfCourse);

// get review by id
reviewRouter.get('/reviews/:reviewId', getReviewById);

// update review
reviewRouter.put("/reviews/:reviewId", updateReviewById);

//delete review
reviewRouter.delete("/courses/:courseId/reviews/:reviewId", deleteReviewById);


module.exports = { reviewRouter };