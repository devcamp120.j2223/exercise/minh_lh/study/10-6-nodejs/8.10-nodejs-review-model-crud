const { request } = require('express');
const { default: mongoose } = require('mongoose');
const reviewModel = require('../models/reviewModel');
const courseModel = require('../models/courseModel');

// create reviews
const createReviewOfCourse = (request, response) => {
    //B1 thu thập thông tin
    let courseId = request.params.courseId;
    let body = request.body;
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is invalid"
        })
    }
    else if (!Number.isInteger(body.stars) || body.stars < 0) {
        response.status(400).json({
            message: "stars is invalid!"
        })
    } else {
        let reviewInput = {
            _id: mongoose.Types.ObjectId(),
            stars: body.stars,
            note: body.note
        }
        //b3 làm việc với cở sở dữ liệu
        reviewModel.create(reviewInput, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                courseModel.findByIdAndUpdate(courseId,
                    {
                        $push: { reviews: data._id }
                    },
                    (err, updatedCourse) => {
                        if (err) {
                            return response.status(500).json({
                                status: "Error 500: Internal server error",
                                message: err.message
                            })
                        } else {
                            return response.status(201).json({
                                status: "Create Review Success",
                                data: data
                            })
                        }
                    }
                )
            }
        })
    }

}
// get All reviews
const getAllReviewOfCourse = (request, response) => {
    // b1 thu thập dữ liệu
    let courseId = request.params.courseId;
    // b2 validate
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is invalid"
        })
    } else {
        //B3: Thao tác với cơ sở dữ liệu
        courseModel.findById(courseId)
            .populate("reviews")
            .exec((error, data) => {
                if (error) {
                    response.status(500).json({
                        message: `Internal server error: ${error.message}`
                    })
                } else {
                    response.status(200).json({
                        data: data.reviews
                    })
                }
            })
    };
}
// get reviews by id
const getReviewById = (request, response) => {
    //B1. thu thập dữ liệu (bỏ qua)
    let reviewId = request.params.reviewId;
    //B2. kiểm tra dữ liệu 
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "id is invalid!"
        })
    } else {
        //B3. thực hiện thao tác dữ liệu
        courseModel.findById(reviewId, (error, data) => {
            if (error) {
                response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
    }
}
const updateReviewById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let reviewId = request.params.reviewId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let reviewUpdate = {
        stars: bodyRequest.rate,
        node: bodyRequest.note
    }

    reviewModel.findByIdAndUpdate(reviewId, reviewUpdate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update review success",
                data: data
            })
        }
    })
}

const deleteReviewById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId;
    let reviewId = request.params.reviewId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Review ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    reviewModel.findByIdAndDelete(reviewId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            // Sau khi xóa xong 1 review khỏi collection cần xóa thêm reviewID trong course đang chứa nó
            courseModel.findByIdAndUpdate(courseId,
                {
                    $pull: { reviews: reviewId }
                },
                (err, updatedCourse) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return response.status(204).json({
                            status: "Success: Delete review success"
                        })
                    }
                })
        }
    })
}
module.exports = {
    createReviewOfCourse: createReviewOfCourse,
    getAllReviewOfCourse: getAllReviewOfCourse,
    getReviewById: getReviewById,
    updateReviewById: updateReviewById,
    deleteReviewById: deleteReviewById.apply,
}