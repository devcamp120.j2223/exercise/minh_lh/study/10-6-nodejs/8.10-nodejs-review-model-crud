//B1: Khai báo thư viện mongoose
const mongoose = require('mongoose');

//B2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//B3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const reviewSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    /*_id : {
      type: mongoose.Types.ObjectId
    },*/
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
});

//B4: Export ra 1 model cho schema
module.exports = mongoose.model("review", reviewSchema);