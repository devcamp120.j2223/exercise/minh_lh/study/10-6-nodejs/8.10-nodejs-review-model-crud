//B1: Khai báo thư viện mongoose
const mongoose = require('mongoose');

//B2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//B3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const courseSchema = new Schema({
    _id : {
      type: mongoose.Types.ObjectId
    },
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        required: true,
        default: 0
    },
    reviews: [
        {
            type: mongoose.Types.ObjectId,
            ref: "reviews"
        }
    ]
});

//B4: Export ra 1 model cho schema
module.exports = mongoose.model("course", courseSchema);